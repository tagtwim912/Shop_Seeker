# Shop seeker

    Ioan SAVOIU
    All Rights Reserved - For non-commercial use only

## Description
Shop seeker is a small project intended for a front-end test using VueJs and Leaflet where you can freely insert geoJSON data and read it with different selectors.
The project was initially a test, it means that most of the features were developed but for a specific given situation but I have decided to turn it into a small project.

Users can use different selectors to pick shops with different selectors and the selected ones are displayed on the shop listing.

There are 4 types of selectors : 
- Area : users can alternate between the province and region (in canada for now), when they click on a province/region, the listing shows the shops that are in the area.
- Lasso : users can freely draw on the map the shape he wants and the interface shows the shops inside.
- Circle : users can select a location on the map and the shops within the radius are selected. Users can also change the radio via the interface and it will automatically update the selection.
- Square : Users have to pick a first corner of the square on the map and a second, then the square/rectangle will draw and the shops inside the shape will display. 

You can freely add a property tag on the shop geojson data such as the Name(at least) and Description to figure them on the listing and on the marker popup.

If you want to add your own shop dataset, you can manually add it in the assets/geoDatasets/shopSets folder (.geojson file extention).

A future work would be to interact with a back server to automatically give new provinces and shops datasets.

*This is my first ever VueJs project so it may have some mistakes or inconsistencies*



## Project setup
### Install dependencies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```


